# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-mobile package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-mobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-28 01:02+0000\n"
"PO-Revision-Date: 2022-04-07 15:04+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/appdrawer/AppDrawerHeader.qml:37
#, kde-format
msgid "Applications"
msgstr "Toepassingen"

#: package/contents/ui/private/ConfigOverlay.qml:98
#, kde-format
msgid "Remove"
msgstr "Verwijderen"

# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vincent Pinon <vpinon@kde.org>, 2017.
# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-28 01:02+0000\n"
"PO-Revision-Date: 2021-10-24 09:15+0200\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.08.1\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/ui/appdrawer/AppDrawerHeader.qml:37
#, kde-format
msgid "Applications"
msgstr "Applications"

#: package/contents/ui/private/ConfigOverlay.qml:98
#, kde-format
msgid "Remove"
msgstr "Supprimer"
